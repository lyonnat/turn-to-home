<?php
  if (isset($_GET['i'])) {
    file_put_contents('ip', base64_encode($_SERVER['REMOTE_ADDR']));
  }
  elseif (file_exists('ip')) {
    $ip = base64_decode(file_get_contents('ip'));
    header("Location: http://{$ip}/");
  }
?>